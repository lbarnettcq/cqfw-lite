public without sharing class cq_OrderBy implements cq_iOrderBy {
    @TestVisible private static final String ORDER_ASC = ' ASC';
    @TestVisible private static final String ORDER_DESC = ' DESC';

    private String m_cq_OrderBy { get; set; }

    public String ForSoql() {
        return m_cq_OrderBy;
    }

    public static cq_OrderBy Ascending(Schema.SObjectField field) {
        return new cq_OrderBy(field, ORDER_ASC);
    }

    public static cq_OrderBy Descending(Schema.SObjectField field) {
        return new cq_OrderBy(field, ORDER_DESC);
    }

    private cq_OrderBy(Schema.SObjectField field, String direction) {
        m_cq_OrderBy = cq_SchemaHelper.GetDescribe(field).getName() + direction;
    }
}