public without sharing class cq_LogHandler {
    public Boolean Buffer { get; set; }
    private List<LogEntry> entries = new List<LogEntry>();
    private static cq_LogHandler m_instance = null;

    private cq_LogHandler() {
        Buffer = false;
    }

    public static cq_LogHandler Instance {
        get {
            if (m_instance == null) {
                m_instance = new cq_LogHandler();
            }
            return m_instance;
        }
    }

    public void Log(System.LoggingLevel level, String message) {
        addLog(level, message);
    }

    public void Error(Exception ex) {
        addLog(System.LoggingLevel.ERROR, cq_ExceptionHelper.FormatVerbose(ex));
    }

    public void Debug(String message) {
        addLog(System.LoggingLevel.DEBUG, message);
    }

    public void Warning(String message) {
        addLog(System.LoggingLevel.FINE, message);
    }

    public void Info(String message) {
        addLog(System.LoggingLevel.INFO, message);
    }

    private void addLog(System.LoggingLevel level, String message) {
        LogEntry entry = new LogEntry(level, message);
        entries.add(entry);
        if (Buffer) { return; }
        commitEntries();
    }

    private void commitEntries() {
        for (LogEntry entry : entries) {
            System.debug(entry.level, entry.message);
        }
        entries = new List<LogEntry>();
    }

    private class LogEntry {
        public System.LoggingLevel level { get; set; }
        public String message { get; set; }

        public LogEntry(System.LoggingLevel severity, String entry) {
            level = severity;
            message = entry;
        }
    }
}