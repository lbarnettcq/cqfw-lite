public inherited sharing virtual class cq {
    public static cq_LogHandler Logger = cq_LogHandler.Instance;

    // UNIT OF WORK BINDING

    private static List<SObjectType> uowPrecedence = new List<SObjectType> {
            Account.SObjectType,
            Contact.SObjectType,
            Contract.SObjectType,
            Opportunity.SObjectType,
            OpportunityContactRole.SObjectType,
            Project__c.SObjectType,
            ContactProject__c.SObjectType,
            OpportunityProjectMapping__c.SObjectType,
            ProjectCreationFieldMapping__c.SObjectType
    };

    public static cq_iUnitOfWork UoW { get { return new cq_UnitOfWork(uowPrecedence); } }

    // START: SELECTOR BINDINGS

    public static AccountsSelector Accounts {
        get { return (AccountsSelector) bindingFor(Account.SObjectType, AccountsSelector.class); }
    }

    public static ContactsSelector Contacts {
        get { return (ContactsSelector) bindingFor(Contact.SObjectType, ContactsSelector.class); }
    }

    public static ContractsSelector Contracts {
        get { return (ContractsSelector) bindingFor(Contract.SObjectType, ContractsSelector.class); }
    }

    public static OpportunitiesSelector Opportunities {
        get { return (OpportunitiesSelector) bindingFor(Opportunity.SObjectType, OpportunitiesSelector.class); }
    }

    public static ProjectsSelector Projects {
        get { return (ProjectsSelector) bindingFor(Project__c.SObjectType, ProjectsSelector.class); }
    }

    public static ContactProjectsSelector ContactProjects {
        get { return (ContactProjectsSelector) bindingFor(ContactProject__c.SObjectType, ContactProjectsSelector.class); }
    }

    public static OpportunityContactRolesSelector OpportunityContactRoles {
        get { return (OpportunityContactRolesSelector) bindingFor(OpportunityContactRole.SObjectType, OpportunityContactRolesSelector.class); }
    }

    public static ProjectCreationFieldMappingsSelector ProjectCreationFieldMappings {
        get { return (ProjectCreationFieldMappingsSelector) bindingFor(ProjectCreationFieldMapping__c.SObjectType, ProjectCreationFieldMappingsSelector.class); }
    }

    public static OpportunityProjectMappingsSelector OpportunityProjectMappings {
        get { return (OpportunityProjectMappingsSelector) bindingFor(OpportunityProjectMapping__c.SObjectType, OpportunityProjectMappingsSelector.class); }
    }

    // END::: SELECTOR BINDINGS

    private static Map<Schema.SObjectType, Object> selector_mappings = new Map<Schema.SObjectType, Object>();
    private static cq_iSObjectSelector bindingFor(Schema.SObjectType sobj, Type target) {
        if (!selector_mappings.containsKey(sobj)) {
            selector_mappings.put(
                    sobj,
                    target.newInstance()
            );
        }
        return (cq_iSObjectSelector) selector_mappings.get(sobj);
    }

    public class CriteriaException extends Exception {}
}