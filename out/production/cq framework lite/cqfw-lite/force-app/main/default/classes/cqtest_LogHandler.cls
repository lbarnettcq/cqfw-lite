@IsTest
private class cqtest_LogHandler {
    @IsTest
    private static void regressionCoverage() {
        cq.Logger.Log(System.LoggingLevel.INFO, 'test manual');
        cq.Logger.Debug('test Debug');
        cq.Logger.Error(new cq.CriteriaException('Exception log test'));
        cq.Logger.Warning('test Warning');
        cq.Logger.Info('test Info');
    }
}